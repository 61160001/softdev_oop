/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author informatics
 */
public class NewEmptyJUnitTest {

    public NewEmptyJUnitTest() {
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }

    @Test
    public void testCheckWinRow1() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.setRowCol(1, 3);
        assertEquals(true, table.checkWin());

    }
    
    public void testCheckWinRow2() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.setRowCol(2, 3);
        assertEquals(true, table.checkWin());

    }
    
    public void testCheckWinRow3() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.setRowCol(3, 1);
        table.setRowCol(3, 2);
        table.setRowCol(3, 3);
        assertEquals(true, table.checkWin());

    }
    
    public void testCheckWinCol1() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.setRowCol(3, 1);
        assertEquals(true, table.checkWin());

    }
    
    public void testCheckWinCol2() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.setRowCol(3, 2);
        assertEquals(true, table.checkWin());

    }
    
    public void testCheckWinCol3() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.setRowCol(1, 3);
        table.setRowCol(2, 3);
        table.setRowCol(3, 3);
        assertEquals(true, table.checkWin());

    }
    
    public void testCheckWinX1() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.setRowCol(3, 3);
        assertEquals(true, table.checkWin());

    }
    
    public void testCheckWinX2() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.setRowCol(3, 1);
        table.setRowCol(2, 2);
        table.setRowCol(1, 3);
        assertEquals(true, table.checkWin());

    }
    
    public void testSetWin() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.setRowCol(1, 3);
        table.checkWin();
        assertEquals(1,table.getWinner().getWin());

    }
    
    public void testSetLose() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.setRowCol(1, 3);
        table.checkWin();
        assertEquals(0,table.getWinner().getLose());

    }
    

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
